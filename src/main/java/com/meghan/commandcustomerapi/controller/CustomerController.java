package com.meghan.commandcustomerapi.controller;

import com.meghan.commandcustomerapi.model.*;
import com.meghan.commandcustomerapi.service.CustomerService;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.concurrent.ExecutionException;

@RestController
@RequestMapping(value = "/cutomers")
@Api(value = "Command Customer", description = "Exposed Endpoints to Register Customer to Food Delivery App, Place order and Submit Feedback", tags = "Command Customer")
public class CustomerController {

    @Autowired
    private CustomerService customerService;

    @PostMapping("/add")
    public CustomerResponse addCustomer(@RequestBody CustomerRequest customer) throws ExecutionException, InterruptedException {
        return customerService.addCustomer(customer);
    }

    @PostMapping("/{customerId}/order")
    public CustomerOrderResponse placeOrder(@RequestBody CustomerOrderRequest request, @PathVariable String customerId) throws ExecutionException, InterruptedException {
        return customerService.placeOrder(request, customerId);
    }

    @PostMapping("/{customerId}/feedback")
    public ResponseEntity addFeedback(@RequestBody CustomerFeedbackRequest request, @PathVariable String customerId) {
        customerService.addFeedback(request, customerId);
        return new ResponseEntity<>(HttpStatus.CREATED);
    }
}
