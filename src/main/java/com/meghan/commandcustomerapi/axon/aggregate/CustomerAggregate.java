package com.meghan.commandcustomerapi.axon.aggregate;

import com.meghan.commandcustomerapi.axon.command.PlaceOrderCommand;
import com.meghan.commandcustomerapi.axon.command.RegisterCustomerCommand;
import com.meghan.commandcustomerapi.axon.command.SubmitFeedbackCommand;
import com.meghan.commandcustomerapi.axon.event.CustomerRegisteredEvent;
import com.meghan.commandcustomerapi.axon.event.FeedbackSubmittedEvent;
import com.meghan.commandcustomerapi.axon.event.OrderPlacedEvent;
import com.meghan.commandcustomerapi.model.MenuItem;
import org.axonframework.commandhandling.CommandHandler;
import org.axonframework.eventsourcing.EventSourcingHandler;
import org.axonframework.modelling.command.AggregateIdentifier;
import org.axonframework.modelling.command.AggregateLifecycle;
import org.axonframework.spring.stereotype.Aggregate;

import java.util.List;

@Aggregate
public class CustomerAggregate {

    @AggregateIdentifier
    private String id;
    private String name;
    private String address;
    private String contactNo;

    private String merchantId;
    private List<MenuItem> menuItems;
    private String status;

    private String feedback;
    private Integer rating;

    CustomerAggregate() {
    }

    @CommandHandler
    public CustomerAggregate(RegisterCustomerCommand command) {
        CustomerRegisteredEvent event = new CustomerRegisteredEvent(command.getId(), command.getName(), command.getAddress(), command.getContactNo());
        AggregateLifecycle.apply(event);
    }

    @EventSourcingHandler
    protected void on(CustomerRegisteredEvent event) {
        this.id = event.getId();
        this.name = event.getName();
        this.address = event.getAddress();
        this.contactNo = event.getContactNo();
    }

    @CommandHandler
    public CustomerAggregate(PlaceOrderCommand command) {
        AggregateLifecycle.apply(new OrderPlacedEvent(command.getId(), command.getMerchantId(), command.getMenuItems(), "IN PROGRESS"));
    }

    @EventSourcingHandler
    protected void on(OrderPlacedEvent event) {
        this.id = event.getId();
        this.merchantId = event.getMerchantId();
        this.status = event.getStatus();
        this.menuItems = event.getMenuItems();
    }

    @CommandHandler
    public CustomerAggregate(SubmitFeedbackCommand command) {
        AggregateLifecycle.apply(new FeedbackSubmittedEvent(command.getId(), command.getFeedback(), command.getRating()));
    }

    @EventSourcingHandler
    protected void on(FeedbackSubmittedEvent event) {
        this.id = event.getId();
        this.feedback = event.getFeedback();
        this.rating = event.getRating();
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getContactNo() {
        return contactNo;
    }

    public void setContactNo(String contactNo) {
        this.contactNo = contactNo;
    }

    public String getMerchantId() {
        return merchantId;
    }

    public void setMerchantId(String merchantId) {
        this.merchantId = merchantId;
    }

    public List<MenuItem> getMenuItems() {
        return menuItems;
    }

    public void setMenuItems(List<MenuItem> menuItems) {
        this.menuItems = menuItems;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getFeedback() {
        return feedback;
    }

    public void setFeedback(String feedback) {
        this.feedback = feedback;
    }

    public Integer getRating() {
        return rating;
    }

    public void setRating(Integer rating) {
        this.rating = rating;
    }
}
