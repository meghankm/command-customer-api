package com.meghan.commandcustomerapi.axon.event;

public class FeedbackSubmittedEvent {

    private final String id;
    private final String feedback;
    private final Integer rating;

    public FeedbackSubmittedEvent(String id, String feedback, Integer rating) {
        this.id = id;
        this.feedback = feedback;
        this.rating = rating;
    }

    public String getId() {
        return id;
    }

    public String getFeedback() {
        return feedback;
    }

    public Integer getRating() {
        return rating;
    }
}
