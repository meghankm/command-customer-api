package com.meghan.commandcustomerapi.axon.event;

public class CustomerRegisteredEvent {
    private final String id;
    private final String name;
    private final String address;
    private final String contactNo;

    public CustomerRegisteredEvent(String id, String name, String address, String contactNo) {
        this.id = id;
        this.name = name;
        this.address = address;
        this.contactNo = contactNo;
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getAddress() {
        return address;
    }

    public String getContactNo() {
        return contactNo;
    }
}
