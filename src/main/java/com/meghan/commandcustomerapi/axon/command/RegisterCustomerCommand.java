package com.meghan.commandcustomerapi.axon.command;

import org.axonframework.modelling.command.TargetAggregateIdentifier;
import org.springframework.beans.factory.annotation.Autowired;

public class RegisterCustomerCommand {

    @TargetAggregateIdentifier
    private final String id;
    private final String name;
    private final String address;
    private final String contactNo;

    @Autowired
    public RegisterCustomerCommand(String id, String name, String address, String contactNo) {
        this.id = id;
        this.name = name;
        this.address = address;
        this.contactNo = contactNo;
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getAddress() {
        return address;
    }

    public String getContactNo() {
        return contactNo;
    }
}
