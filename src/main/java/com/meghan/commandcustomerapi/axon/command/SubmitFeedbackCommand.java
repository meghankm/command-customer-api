package com.meghan.commandcustomerapi.axon.command;

import org.axonframework.modelling.command.TargetAggregateIdentifier;

public class SubmitFeedbackCommand {

    @TargetAggregateIdentifier
    private final String id;
    private final String feedback;
    private final Integer rating;

    public SubmitFeedbackCommand(String id, String feedback, Integer rating) {
        this.id = id;
        this.feedback = feedback;
        this.rating = rating;
    }

    public String getId() {
        return id;
    }

    public String getFeedback() {
        return feedback;
    }

    public Integer getRating() {
        return rating;
    }
}
