package com.meghan.commandcustomerapi.axon.command;

import com.meghan.commandcustomerapi.model.MenuItem;
import org.axonframework.modelling.command.TargetAggregateIdentifier;

import java.util.List;

public class PlaceOrderCommand {

    @TargetAggregateIdentifier
    private final String id;
    private final String merchantId;
    private final List<MenuItem> menuItems;

    public PlaceOrderCommand(String id, String merchantId, List<MenuItem> menuItems) {
        this.id = id;
        this.merchantId = merchantId;
        this.menuItems = menuItems;
    }

    public String getId() {
        return id;
    }

    public String getMerchantId() {
        return merchantId;
    }

    public List<MenuItem> getMenuItems() {
        return menuItems;
    }
}
