package com.meghan.commandcustomerapi.service;


import com.meghan.commandcustomerapi.model.*;

import java.util.concurrent.ExecutionException;

public interface CustomerService {
    CustomerResponse addCustomer(CustomerRequest customer) throws ExecutionException, InterruptedException;

    CustomerOrderResponse placeOrder(CustomerOrderRequest customerOrder, String customerId) throws ExecutionException, InterruptedException;

    void addFeedback(CustomerFeedbackRequest feedback, String customerId);
}
