package com.meghan.commandcustomerapi.service;

import com.meghan.commandcustomerapi.axon.command.PlaceOrderCommand;
import com.meghan.commandcustomerapi.axon.command.RegisterCustomerCommand;
import com.meghan.commandcustomerapi.axon.command.SubmitFeedbackCommand;
import com.meghan.commandcustomerapi.axon.event.CustomerRegisteredEvent;
import com.meghan.commandcustomerapi.axon.event.OrderPlacedEvent;
import com.meghan.commandcustomerapi.model.*;
import com.meghan.commandcustomerapi.rabbitmq.MQSender;
import org.axonframework.commandhandling.gateway.CommandGateway;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.UUID;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

@Service
public class CustomerServiceImpl implements CustomerService {

    @Autowired
    private CommandGateway commandGateway;

    @Autowired
    private MQSender mqSender;

    @Override
    public CustomerResponse addCustomer(CustomerRequest customer) throws ExecutionException, InterruptedException {
        CompletableFuture<String> customerIdFuture = commandGateway.send(new RegisterCustomerCommand(UUID.randomUUID().toString(), customer.getName(), customer.getAddress(), customer.getContactNo()));

        return !customerIdFuture.isCompletedExceptionally() ? new CustomerResponse(customerIdFuture.get()) : null;
    }

    @Override
    public CustomerOrderResponse placeOrder(CustomerOrderRequest customerOrder, String customerId) throws ExecutionException, InterruptedException {
        String merchantId = UUID.randomUUID().toString();
        OrderPlacedEvent event = new OrderPlacedEvent(customerId, merchantId, customerOrder.getMenuItems(), "IN PROGRESS");
        mqSender.sendOrderPlacedEvent(event);

        CompletableFuture<String> deliveryIdFuture = commandGateway.send(new PlaceOrderCommand(merchantId, customerOrder.getMerchantId(), customerOrder.getMenuItems()));

        return !deliveryIdFuture.isCompletedExceptionally() ? new CustomerOrderResponse(deliveryIdFuture.get(), customerId, "IN PROGRESS") : null;
    }

    @Override
    public void addFeedback(CustomerFeedbackRequest feedback, String customerId) {
        commandGateway.send(new SubmitFeedbackCommand(UUID.randomUUID().toString(), feedback.getFeedback(), feedback.getRating()));
    }
}
