package com.meghan.commandcustomerapi.rabbitmq;

import com.meghan.commandcustomerapi.axon.event.OrderPlacedEvent;
import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
public class MQSender {

    @Autowired
    private AmqpTemplate rabbitTemplate;

    @Value("${customer.rabbitmq.exchange}")
    private String exchange;

    @Value("${customer.rabbitmq.routingkey}")
    private String routingkey;

    public void sendOrderPlacedEvent(OrderPlacedEvent event) {
        rabbitTemplate.convertAndSend(exchange, routingkey, event);
        System.out.println("Send msg = " + event);

    }
}
