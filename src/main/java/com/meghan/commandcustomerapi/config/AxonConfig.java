package com.meghan.commandcustomerapi.config;

        import com.meghan.commandcustomerapi.axon.aggregate.CustomerAggregate;
        import org.axonframework.eventsourcing.EventSourcingRepository;
        import org.axonframework.eventsourcing.eventstore.EventStore;
        import org.springframework.context.annotation.Bean;
        import org.springframework.context.annotation.Configuration;

@Configuration
public class AxonConfig {

    @Bean
    EventSourcingRepository<CustomerAggregate> customerAggregateEventSourcingRepository(EventStore eventStore) {
        EventSourcingRepository<CustomerAggregate> repository = EventSourcingRepository.builder(CustomerAggregate.class)
                .eventStore(eventStore)
                .build();

        return repository;
    }
}
