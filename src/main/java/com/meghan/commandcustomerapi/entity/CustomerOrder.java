package com.meghan.commandcustomerapi.entity;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class CustomerOrder {

    @Id
    private String id;
    private String merchantId;
    private String status;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getMerchantId() {
        return merchantId;
    }

    public void setMerchantId(String merchantId) {
        this.merchantId = merchantId;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
