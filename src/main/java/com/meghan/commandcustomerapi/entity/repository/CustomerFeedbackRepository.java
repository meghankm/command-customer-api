package com.meghan.commandcustomerapi.entity.repository;

import com.meghan.commandcustomerapi.entity.Feedback;
import org.springframework.data.repository.CrudRepository;

public interface CustomerFeedbackRepository extends CrudRepository<Feedback, String> {
}
