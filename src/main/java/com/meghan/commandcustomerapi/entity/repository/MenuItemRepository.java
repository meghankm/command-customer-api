package com.meghan.commandcustomerapi.entity.repository;

import com.meghan.commandcustomerapi.entity.MenuItem;
import org.springframework.data.repository.CrudRepository;

public interface MenuItemRepository extends CrudRepository<MenuItem, String> {
}

