package com.meghan.commandcustomerapi.entity.repository;

import com.meghan.commandcustomerapi.entity.CustomerOrder;
import org.springframework.data.repository.CrudRepository;

public interface CustomerOrderRepository extends CrudRepository<CustomerOrder, String> {
}

