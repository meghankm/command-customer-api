package com.meghan.commandcustomerapi.entity.repository;

        import com.meghan.commandcustomerapi.entity.Customer;
        import org.springframework.data.repository.CrudRepository;

public interface CustomerRepository extends CrudRepository<Customer, String> {
}
