package com.meghan.commandcustomerapi.entity.handler;

import com.meghan.commandcustomerapi.axon.aggregate.CustomerAggregate;
import com.meghan.commandcustomerapi.axon.event.CustomerRegisteredEvent;
import com.meghan.commandcustomerapi.axon.event.FeedbackSubmittedEvent;
import com.meghan.commandcustomerapi.axon.event.OrderPlacedEvent;
import com.meghan.commandcustomerapi.entity.Customer;
import com.meghan.commandcustomerapi.entity.CustomerOrder;
import com.meghan.commandcustomerapi.entity.Feedback;
import com.meghan.commandcustomerapi.entity.MenuItem;
import com.meghan.commandcustomerapi.entity.repository.CustomerFeedbackRepository;
import com.meghan.commandcustomerapi.entity.repository.CustomerOrderRepository;
import com.meghan.commandcustomerapi.entity.repository.CustomerRepository;
import com.meghan.commandcustomerapi.entity.repository.MenuItemRepository;
import org.axonframework.eventsourcing.EventSourcingHandler;
import org.axonframework.eventsourcing.EventSourcingRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

@Component
public class CustomerEntityManager {

    @Autowired
    private CustomerRepository customerRepository;

    @Autowired
    private CustomerOrderRepository customerOrderRepository;

    @Autowired
    private MenuItemRepository menuItemRepository;

    @Autowired
    private CustomerFeedbackRepository customerFeedbackRepository;

    @Autowired
    @Qualifier("customerAggregateEventSourcingRepository")
    private EventSourcingRepository<CustomerAggregate> customerAggregateEventSourcingRepository;

    @EventSourcingHandler
    void on(CustomerRegisteredEvent event) {
        CustomerAggregate customerAggregate = customerAggregateEventSourcingRepository.load(event.getId())
                .getWrappedAggregate()
                .getAggregateRoot();

        Customer customer = new Customer();
        customer.setId(customerAggregate.getId());
        customer.setName(customerAggregate.getName());
        customer.setAddress(customerAggregate.getAddress());
        customer.setContactNo(customerAggregate.getContactNo());

        customerRepository.save(customer);
    }

    @EventSourcingHandler
    void on(OrderPlacedEvent event) {
        CustomerAggregate customerAggregate = customerAggregateEventSourcingRepository.load(event.getId())
                .getWrappedAggregate()
                .getAggregateRoot();

        CustomerOrder customer = new CustomerOrder();
        customer.setId(customerAggregate.getId());
        customer.setMerchantId(customerAggregate.getMerchantId());
        customer.setStatus(customerAggregate.getStatus());

        customerOrderRepository.save(customer);

        customerAggregate.getMenuItems().forEach(
                menuItem -> {
                    MenuItem item = new MenuItem();
                    item.setId(menuItem.getId());
                    item.setMenuItem(menuItem.getMenuItem());
                    item.setAmount(menuItem.getPrice().getAmount());
                    item.setCurrency(menuItem.getPrice().getCurrency());
                    item.setCategory(menuItem.getCategory());
                    menuItemRepository.save(item);
                }
        );
    }

    @EventSourcingHandler
    void on(FeedbackSubmittedEvent event) {
        CustomerAggregate customerAggregate = customerAggregateEventSourcingRepository.load(event.getId())
                .getWrappedAggregate()
                .getAggregateRoot();

        Feedback feedback = new Feedback();
        feedback.setId(customerAggregate.getId());
        feedback.setFeedback(customerAggregate.getFeedback());
        feedback.setRating(customerAggregate.getRating());

        customerFeedbackRepository.save(feedback);
    }
}




