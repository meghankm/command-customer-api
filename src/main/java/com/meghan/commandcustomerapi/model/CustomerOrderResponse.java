package com.meghan.commandcustomerapi.model;

public class CustomerOrderResponse {

    private String deliveryId;
    private String customerId;
    private String status;

    public CustomerOrderResponse(String deliveryId, String customerId, String status) {
        this.deliveryId = deliveryId;
        this.customerId = customerId;
        this.status = status;
    }

    public String getDeliveryId() {
        return deliveryId;
    }

    public void setDeliveryId(String deliveryId) {
        this.deliveryId = deliveryId;
    }

    public String getCustomerId() {
        return customerId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
