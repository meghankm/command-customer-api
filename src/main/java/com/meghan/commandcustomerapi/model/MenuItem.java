package com.meghan.commandcustomerapi.model;

public class MenuItem {

    private String id;
    private String menuItem;
    private Price price;
    private String category;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getMenuItem() {
        return menuItem;
    }

    public void setMenuItem(String menuItem) {
        this.menuItem = menuItem;
    }

    public Price getPrice() {
        return price;
    }

    public void setPrice(Price price) {
        this.price = price;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    @Override
    public String toString() {
        return "MenuItem [ id=" + id + ", menuItem=" + menuItem + ", category=" + category + ", price=" + price + " ]";
    }
}


