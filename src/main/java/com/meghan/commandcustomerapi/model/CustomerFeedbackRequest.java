package com.meghan.commandcustomerapi.model;

public class CustomerFeedbackRequest {

    private String feedback;
    private Integer rating;

    public String getFeedback() {
        return feedback;
    }

    public void setFeedback(String feedback) {
        this.feedback = feedback;
    }

    public Integer getRating() {
        return rating;
    }

    public void setRating(Integer rating) {
        this.rating = rating;
    }
}
