package com.meghan.commandcustomerapi.model;

import java.util.List;

public class CustomerOrderRequest {

    private String merchantId;
    private List<MenuItem> menuItems;

    public String getMerchantId() {
        return merchantId;
    }

    public void setMerchantId(String merchantId) {
        this.merchantId = merchantId;
    }

    public List<MenuItem> getMenuItems() {
        return menuItems;
    }

    public void setMenuItems(List<MenuItem> menuItems) {
        this.menuItems = menuItems;
    }
}
